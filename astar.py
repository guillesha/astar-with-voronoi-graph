"""Execution of A* algorithm in python"""

import math


class Node:
    """Defines a node of the graph"""

    def __init__(self, label, x, y):
        self.label = label
        self.x = x
        self.y = y
        self.neighbors = []

    def set_neighbors(self, neighbors):
        self.neighbors = neighbors

    def __str__(self):
        return self.label


def euclidean_distance(a: Node, b: Node) -> float:
    """Calculates the euclidean distance betwwen two nodes
    :param a coordinates of node a
    :param b coordinates of nod b
    :return float representing distance"""
    return math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.y - b.y, 2))


def f_function(node: Node, initial_node: Node, goal_node: Node):
    """Calculates the value of f(n)"""
    return g_function(initial_node, node) + h_function(node, goal_node)


def h_function(node: Node, goal_node: Node) -> float:
    """:returns the expection h* for the node"""
    return euclidean_distance(node, goal_node)


def g_function(initial_goal: Node, target_node: Node):
    """Returns the cost from the initial node to the next one  """
    a = target_node
    euclidean_distance(initial_goal, target_node)
    return euclidean_distance(initial_goal, target_node)


# Implemntation of a star algorithm
def astar(initial_node: Node, goal_node: Node):
    """Applies a star algorithm"""
    current_node = initial_node
    list_of_nodes = [initial_node]

    while current_node != goal_node:
        # While we are not on goal algorithm
        for visited in list_of_nodes:
            if visited in current_node.neighbors:
                current_node.neighbors.remove(visited)
        better_euclidean = f_function(current_node.neighbors[0], initial_node, goal_node)

        for neighbour in current_node.neighbors:
            if f_function(neighbour, initial_node, goal_node) <= better_euclidean:
                # if f_function is better set this node as the best and go to this node

                better_euclidean = f_function(neighbour, initial_node, goal_node)
                current_node = neighbour
        list_of_nodes.append(current_node)
    return list_of_nodes
