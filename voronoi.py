# Make sure to have the server side running in V-REP:
# in a child script of a V-REP scene, add following command
# to be executed just once, at simulation start:
#
# simRemoteApi.start(19999)
#
# then start simulation, and run this program.
#
# IMPORTANT: for each successful call to simxStart, there
# should be a corresponding call to simxFinish at the end!
import atexit
import math
from astar import Node, astar, f_function

try:
    import vrep
except:
    print('--------------------------------------------------------------')
    print('"vrep.py" could not be imported. This means very probably that')
    print('either "vrep.py" or the remoteApi library could not be found.')
    print('Make sure both are in the same folder as this file,')
    print('or appropriately adjust the file "vrep.py"')
    print('--------------------------------------------------------------')
    print('')

import time

NODE_F = Node("F", -2.52, 3.25)
NODE_G = Node("G", -2.9, 2.57)
NODE_H = Node("H", -2.55, 1.9)
NODE_E = Node("E", -1.47, 3.92)
NODE_C = Node("C", 0, 2.45)
NODE_B = Node("B", 0.05, 1.25)
NODE_D = Node("D", -0.1, 3.37)
NODE_A = Node("A", -1.25, 1.07)

NODE_A.set_neighbors([NODE_B, NODE_H])
NODE_B.set_neighbors([NODE_C, NODE_A])
NODE_C.set_neighbors([NODE_B, NODE_D])
NODE_D.set_neighbors([NODE_C, NODE_E])
NODE_E.set_neighbors([NODE_D, NODE_F])
NODE_F.set_neighbors([NODE_E, NODE_G])
NODE_G.set_neighbors([NODE_F, NODE_H])
NODE_H.set_neighbors([NODE_A, NODE_G])

LIST_OF_NODES = astar(NODE_A, NODE_E)
INITIAL_NODE = NODE_A
GOAL_NODE = NODE_E


def exit_handler():
    """Stops the simulation when stops the program"""
    vrep.simxAuxiliaryConsoleClose(clientID, my_console, vrep.simx_opmode_blocking)
    vrep.simxStopSimulation(clientID, vrep.simx_opmode_blocking)


atexit.register(exit_handler)
Robot = "Pioneer_p3dx"
Spotlight = "Spotlight"


def get_distance_from_node(robot_position, node):
    """:return the distance between the robot and the node"""
    return math.sqrt(
        math.pow(node.x - robot_position[0], 2) + math.pow(node.y - robot_position[1], 2))


def get_angle_from_node(robot_position, node):
    """:return the angle from next node"""
    return math.atan2(node.y - robot_position[1], node.x - robot_position[0])


def set_robot_velocity(v_left, v_right):
    """Sets the robot velocity"""
    error_code_velocity = vrep.simxSetJointTargetVelocity(clientID, left_motor, v_left,
                                                          vrep.simx_opmode_streaming)
    error_code_velocity_right = vrep.simxSetJointTargetVelocity(clientID, right_motor, v_right,
                                                                vrep.simx_opmode_streaming)


print('Program started')
vrep.simxFinish(-1)  # just in case, close all opened connections
clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 5000, 5)  # Connect to V-REP
if clientID != -1:
    print('Connected to remote API server')
    # left motor of the robot

    error_code, left_motor = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_leftMotor",
                                                      vrep.simx_opmode_blocking)

    error_code2, right_motor = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_rightMotor",
                                                        vrep.simx_opmode_blocking)
    error_code3, floorHandle = vrep.simxGetObjectHandle(clientID, 'ResizableFloor_5_25',
                                                        vrep.simx_opmode_blocking)

    error_code4, robot_handle = vrep.simxGetObjectHandle(clientID, Robot,
                                                         vrep.simx_opmode_blocking)
    error_code5, position = vrep.simxGetObjectPosition(clientID, robot_handle, floorHandle, vrep.simx_opmode_streaming)
    vrep.simxStartSimulation(clientID, vrep.simx_opmode_streaming)

    error_code_goal, spotlight_handle = vrep.simxGetObjectHandle(clientID, Spotlight,
                                                                 vrep.simx_opmode_blocking)
    error_code6, noad_position = vrep.simxGetObjectPosition(clientID, spotlight_handle, floorHandle,
                                                            vrep.simx_opmode_streaming)

    error_code9, my_console = vrep.simxAuxiliaryConsoleOpen(clientID, 'Text_Debugger', 10, 0, [], [400, 400], None,
                                                            None,
                                                            vrep.simx_opmode_blocking)
    vrep.simxAuxiliaryConsolePrint(clientID, my_console,
                                   "Calculate route from " + INITIAL_NODE.label + " to " + GOAL_NODE.label + ".",
                                   vrep.simx_opmode_blocking)
    vrep.simxAuxiliaryConsolePrint(clientID, my_console,
                                   "\nA* SAYS BEST ROUTE IS " + str([str(node) for node in LIST_OF_NODES]) + "",
                                   vrep.simx_opmode_blocking)

    i = 0
    current_node = LIST_OF_NODES[i]

    while True:
        orientation = \
            vrep.simxGetObjectOrientation(clientID, robot_handle, spotlight_handle, vrep.simx_opmode_streaming)[1][
                2]
        code, current_position = vrep.simxGetObjectPosition(clientID, robot_handle, -1,
                                                            vrep.simx_opmode_streaming)

        code2, spotlight_position = vrep.simxGetObjectPosition(clientID, spotlight_handle, floorHandle,
                                                               vrep.simx_opmode_buffer)
        set_robot_velocity(0, 0)
        changing_angle = math.degrees(orientation) - math.degrees(get_angle_from_node(current_position, current_node))

        if get_distance_from_node(current_position, current_node) > 0.2:
            if abs(changing_angle) - 180 > 0:
                if abs(changing_angle) - 180 < 5:

                    set_robot_velocity(5, 5)
                else:
                    set_robot_velocity(0, 1)
            else:
                if abs(changing_angle) - 180 > -20:

                    set_robot_velocity(5, 5)
                else:

                    set_robot_velocity(1, 0)


        else:
            set_robot_velocity(0, 0)

            i += 1
            vrep.simxAuxiliaryConsolePrint(clientID, my_console, "\nREACHED NODE " + current_node.label,
                                           vrep.simx_opmode_blocking)

            if current_node == GOAL_NODE:
                vrep.simxAuxiliaryConsolePrint(clientID, my_console, "\n REACHED GOAL NODE",
                                               vrep.simx_opmode_blocking)

                time.sleep(10)
                vrep.simxAuxiliaryConsoleClose(clientID, my_console, vrep.simx_opmode_blocking)
                vrep.simxStopSimulation(clientID, vrep.simx_opmode_blocking)
            else:
                current_node = LIST_OF_NODES[i]










else:
    print('Failed connecting to remote API server')
print('Program ended')
